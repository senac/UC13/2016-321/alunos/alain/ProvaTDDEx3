package br.com.senac.ex3;

import org.junit.Assert;
import org.junit.Test;

public class ConverterMoedaTest {

    @Test
    public void deveConverter5ReaisParaDolar() {
        double valorOrigem = 5;
        ConverterMoeda calculo = new ConverterMoeda ();
        double valorResultado = calculo.ConverterRealParaDolar(valorOrigem);
        Assert.assertEquals(1.54, valorResultado, 0.01);

    }    
    
     @Test
    public void deveConverter5ReaisParaDolarAustraliano() {
        double valorOrigem = 5;
        ConverterMoeda calculo = new ConverterMoeda ();
        double valorResultado = calculo.ConverterRealParaDolarAustraliano(valorOrigem);
        Assert.assertEquals(2.02, valorResultado, 0.01);

    }    
    
     @Test
    public void deveConverter5ReaisParaEuro() {
        double valorOrigem = 5;
        ConverterMoeda calculo = new ConverterMoeda ();
        double valorResultado = calculo.ConverterRealParaEuro(valorOrigem);
        Assert.assertEquals(1.30, valorResultado, 0.01);

    }    

}

