
package br.com.senac.ex3;


public class ConverterMoeda {
    
    private static final double BRL = 5.00;
    private static  final double USD = 3.23;
    private static final double AUD = 2.47;
    private static  final double EUR = 3.85;    
    
    public double ConverterRealParaDolar(double BRL){
        return BRL / USD;
    }
    
    public double ConverterRealParaDolarAustraliano(double BRL){
        return BRL / AUD;
    }
    
    public double ConverterRealParaEuro(double BRL){
        return BRL / EUR;
    }
}    
    